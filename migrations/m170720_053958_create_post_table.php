<?php

use yii\db\Migration;

/**
 * Handles the creation of table `post`.
 */
class m170720_053958_create_post_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('post', [
            'id' => $this->primaryKey(),
			'title' => 'string',
			'body' => 'string',
			'category' => 'integer',
			'author' => 'integer',
			'status' => 'integer',
			'created_at' => 'integer',
			'updated_at' => 'integer',
			'created_by' => 'integer',
			'updated_by' => 'integer',
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('post');
    }
}
