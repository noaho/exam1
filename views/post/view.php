<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Post */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Posts', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="post-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'title',
            'body',
            //'category',
            //'author',
			   [
				'attribute' => 'author',
				'label' => 'Author',
				'value' => function($model){
							return $model->author->name;
					},		
			],
            //'status',
            'created_at',
            'updated_at',
            'created_by',
            'updated_by',
			/*['label' => $model->attributeLabels()['status'],
				'value' => $model->statusItem->'status_name'],*/
        ],
    ]) ?>

</div>
